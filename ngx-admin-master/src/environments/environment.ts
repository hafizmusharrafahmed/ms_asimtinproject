/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead. http://localhost:49316/api
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  isMockEnabled: true, // You have to switch this, when your real back-end is done
  authTokenKey: "authce9d77b308c149d5992a80073637e4d5",
  //apiUrl: "http://localhost:49316/api/",
  apiUrl: "http://localhost:63808/api/",
  apiReportUrl: "http://localhost:63808/",
  apiFileDownloadUrl: "http://localhost:63808/UploadDocuments/"
};
