import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { ThemeModule } from "../../@theme/theme.module";
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import {
  NbActionsModule,
  NbButtonModule,
  NbCardModule,
  NbCheckboxModule,
  NbDatepickerModule,
  NbIconModule,
  NbInputModule,
  NbRadioModule,
  NbSelectModule,
  NbUserModule,
} from "@nebular/theme";

import { jqxGridModule } from "jqwidgets-ng/jqxgrid";
const moduleArray = [
  CommonModule,
  FormsModule,
  ReactiveFormsModule,
  // jqx grid module is import for temporary
  jqxGridModule,
  ThemeModule,
  NbCardModule,
  NbUserModule,
  NbButtonModule,
  NbActionsModule,
  NbRadioModule,
  NbIconModule,
  NbButtonModule,
  NbInputModule,
  NbCardModule,
  NbButtonModule,
  NbActionsModule,
  NbUserModule,
  NbCheckboxModule,
  NbRadioModule,
  NbDatepickerModule,
  NbSelectModule,
  NbIconModule,
];
@NgModule({
  imports: [moduleArray],
  declarations: [],
  exports: [moduleArray],

  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class shareModule {}
