export class returnModel {
  Message: string;
  Success: boolean;
  StatusCode: number;
  DataObject: any;
}

export interface IFOrString {
  id: string;
  name: string;
}
export interface IForNumber {
  id: number;
  name: string;
}
export const typeData: IForNumber[] = [
  { name: "Apps", id: 3 },
  { name: "Admin", id: 2 },
  { name: "Other", id: 5 },
  { name: "Manager", id: 4 },
  { name: "RD/Agent", id: 1 },
  { name: "GCD Agent", id: 6 }
];
export const Depert: IFOrString[] = [
  { name: "All", id: "Z" },
  { name: "IT", id: "B" },
  { name: "Admin", id: "A" },
  { name: "Career", id: "E" },
  { name: "Account", id: "C" },
  { name: "Supply One", id: "F" }
];
export const statGroup: IFOrString[] = [
  { name: "A/R", id: "Z" },
  { name: "ABC", id: "B" },
  { name: "ACCES", id: "A" },
  { name: "BADDERT", id: "E" },
  { name: "CMOP", id: "C" },
  { name: "ALL TYPE", id: "F" }
];
export const checkList: IFOrString[] = [
  { name: "All", id: "All" },
  { name: "Active", id: "ACTIVE" },
  { name: "Not Active", id: "NOT ACTIVE" }
];
export const steps: IFOrString[] = [
  { name: "Instruction", id: "Instruction" },
  { name: "Manual Steps", id: "Manual Steps" }
];
