import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { HttpEvent, HttpRequest, HttpResponse } from "@angular/common/http";

import { Observable, of } from "rxjs";
import { APP_CONFIG } from "../app.config";
import { catchError, map } from "rxjs/operators";
import { tap } from "rxjs/operators";
//import { ToastrService } from "ngx-toastr";

@Injectable({
  providedIn: "root"
})
export class genericService {
  public token: string;

  constructor(
    private http: HttpClient //private toastr: ToastrService
  ) {}
  gettoken() {
    let token_id = JSON.parse(localStorage.getItem("currUserr"));

    this.token = token_id != null ? token_id.Token : "";
    return this.token;
  }

  service(method: string, body: any) {
    if (method !== "") {
      const httpOptions = {
        headers: new HttpHeaders({
          "Content-Type": "application/json"
          //Authorization: "Bearer " + this.gettoken()
        })
      };

      let url = APP_CONFIG.BaseUrl + method;
      //console.log(url);
      return this.http.post(url, body, httpOptions).pipe(
        tap(
          event => {
            if (event instanceof HttpResponse) {
              // console.log('all looks good');
              // http response status code
              // console.log(event.status); 400,404,500
            }
          },
          error => {
            if (error.status == 0) {
              //this.toastr.warning(error.message);
            } else if (error.status === 500) {
              // this.toastr.warning(error.message);
            } else if (error.status === 400) {
              //this.toastr.warning(error.message);
            } else if (error.status === 404) {
              //this.toastr.warning(error.message);
            } else if (error.status === 401) {
              //this.toastr.warning(error.message);
            } else {
              return error;
            }
          }
        )
      );
    }
  }
  login(method: string, body: any) {
    if (method !== "") {
      const httpOptions = {
        headers: new HttpHeaders({
          "Content-Type": "application/json"
        })
      };

      let url = APP_CONFIG.BaseUrl + method;
      //console.log(url);
      return this.http.post(url, body, httpOptions).pipe(
        tap(
          event => {
            if (event instanceof HttpResponse) {
              // console.log('all looks good');
              // http response status code
              // console.log(event.status); 400,404,500
            }
          },
          error => {
            if (error.status == 0) {
            } else if (error.status === 500) {
              //this.toastr.warning(error.message);
            } else if (error.status === 400) {
              //this.toastr.warning(error.message);
            } else if (error.status === 404) {
              //this.toastr.warning(error.message);
            } else if (error.status === 401) {
              // this.toastr.warning(error.message);
            } else {
              return error;
            }
          }
        )
      );
    }
  }
  // it is used for get user service
  getService(method: string) {
    if (method !== "") {
      const httpOptions = {
        headers: new HttpHeaders({
          "Content-Type": "application/json"
          //Authorization: "Bearer " + this.gettoken()
        })
      };

      let url = APP_CONFIG.BaseUrl + method;
      //console.log(url);
      return this.http.get(url, httpOptions).pipe(
        tap(
          event => {
            if (event instanceof HttpResponse) {
              // console.log('all looks good');
              // http response status code
              // console.log(event.status); 400,404,500
            }
          },
          error => {
            if (error.status == 0) {
              //this.toastr.warning(error.message);
            } else if (error.status === 500) {
              //this.toastr.warning(error.message);
            } else if (error.status === 400) {
              //this.toastr.warning(error.message);
            } else if (error.status === 404) {
              //this.toastr.warning(error.message);
            } else if (error.status === 401) {
              //this.toastr.warning(error.message);
            } else {
              return error;
            }
          }
        )
      );
    }
  }

  private handleError<T>(operation = "operation", result?: any) {
    return (error: any): Observable<any> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result);
    };
  }
  dateFormat(inputDate) {
    if (inputDate != null) {
      if (inputDate.length == 10) inputDate += " 00:00:00";
      var date = new Date(inputDate);
      if (!isNaN(date.getTime())) {
        if (date.getFullYear() < 1901) return "";

        // Months use 0 index.
        let Month = "";
        let b;

        if (date.getMonth() + 1 < 9) {
          b = date.getMonth() + 1;
          Month = "0" + b;
        } else {
          Month = (date.getMonth() + 1).toString();
        }
        let ddate = "";
        if (date.getDate() < 9) {
          ddate = "0" + date.getDate();
        } else {
          ddate = date.getDate().toString();
        }

        return Month + "/" + ddate + "/" + date.getFullYear();
      } else {
        return "";
      }
    } else {
      return "";
    }
  }

  dateFormatForbegindata(inputDate) {
    if (inputDate != null) {
      if (inputDate.length == 10) inputDate += " 00:00:00";
      var date = new Date(inputDate);
      if (!isNaN(date.getTime())) {
        if (date.getFullYear() < 1900) return "";

        // Months use 0 index.
        let Month = "";
        let b;

        if (date.getMonth() + 1 < 9) {
          b = date.getMonth() + 1;
          Month = "0" + b;
        } else {
          Month = (date.getMonth() + 1).toString();
        }
        let ddate = "";
        if (date.getDate() < 9) {
          ddate = "0" + date.getDate();
        } else {
          ddate = date.getDate().toString();
        }

        return Month + "/" + ddate + "/" + date.getFullYear();
      } else {
        return "";
      }
    } else {
      return "";
    }
  }

  FormateDatetimeForLocal(GetDate) {
    let s = GetDate.toString();
    if (s.includes("AM") || s.includes("PM")) {
      return GetDate;
    }
    const D1 = GetDate.split("T");
    const Dat = D1[0];
    let FormateDate = Dat.split("-");
    let FinalDate =
      FormateDate[1] + "/" + FormateDate[2] + "/" + FormateDate[0];
    var Tm = D1[1].split(".");
    Tm = Tm[0];
    var TSplit = Tm.split(":");
    var Hour = 0;
    if (TSplit[0] > 12) {
      Hour = TSplit[0] - 12;
      Tm = Hour + ":" + TSplit[1] + " " + "PM";
    } else {
      Tm = TSplit[0] + ":" + TSplit[1] + " " + "AM";
    }
    if (FinalDate == "01/01/1900") {
      return "";
    }
    var d = FinalDate + " " + Tm; //new Date(FinalDate + " " + Tm + " UTC");
    return d;
  }
  public downloadFile(method: string, body: any): Observable<HttpEvent<Blob>> {
    let url = APP_CONFIG.BaseUrl + method;
    const httpOptions = {};
    return this.http.request(
      new HttpRequest("POST", `${url}`, body, {
        headers: new HttpHeaders({
          "Content-Type": "application/json",
          Authorization: "Bearer " + this.gettoken()
        }),
        reportProgress: true,
        responseType: "blob"
      })
    );
  }
  public ReportFile(method: string, body: any): Observable<HttpEvent<Blob>> {
    let url = APP_CONFIG.ReportUrl + method;
    const httpOptions = {};
    return this.http.request(
      new HttpRequest("POST", `${url}`, body, {
        headers: new HttpHeaders({
          "Content-Type": "application/json",
          Authorization: "Bearer " + this.gettoken()
        }),
        reportProgress: true,
        responseType: "blob"
      })
    );
  }
  ShowFilePost(method: string, body: any): any {
    let url = APP_CONFIG.ReportUrl + method;

    return this.http
      .post(url, body, {
        headers: new HttpHeaders({
          "Content-Type": "application/json",
          Authorization: "Bearer " + this.gettoken()
        }),
        responseType: "blob"
      })
      .pipe(
        map((result: any) => {
          //
          return result;
        })
      );
  }
  removeDollarSign(getvalue: string) {
    let newValue: any = 0;
    //debugger;
    if (getvalue.includes("$")) {
      newValue = getvalue.toString().substring(1);
    }
    return Number(newValue);
  }
}
