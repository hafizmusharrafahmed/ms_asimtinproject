import { environment } from "../../environments/environment";

export const APP_CONFIG = {
  BaseUrl: environment.apiUrl,
  ReportUrl: environment.apiReportUrl,
  FileDownloadUrl: environment.apiFileDownloadUrl
};
