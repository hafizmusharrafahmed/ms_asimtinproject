import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { genericService } from "./../../@core/service/genericService";
import { Component, OnDestroy, OnInit, ViewChild } from "@angular/core";

import pdfMake from "pdfmake/build/pdfmake";
import pdfFonts from "pdfmake/build/vfs_fonts";
import { addrepgridcomponent } from "../forms/form-layouts/addrep/addrepgrid.component";
import { returnModel } from "../../@core/service/HandlerModel";
pdfMake.vfs = pdfFonts.pdfMake.vfs;

interface CardSettings {
  title: string;
  iconClass: string;
  type: string;
}

@Component({
  selector: "ngx-dashboard",
  styleUrls: ["./dashboard.component.scss"],
  templateUrl: "./dashboard.component.html",
  providers: [genericService],
})
export class DashboardComponent implements OnDestroy, OnInit {
  private alive = true;
  @ViewChild(addrepgridcomponent) mygrid: addrepgridcomponent;
  solarValue: number;

  BankAccount: FormGroup;

  constructor(
    private _genericService: genericService,

    private fb: FormBuilder
  ) {}
  patchValue(event) {
    this.BankAccount.patchValue({ dcid: event.DCNO + 1 });
  }

  submitForm() {
    let row = this.mygrid.myGrid.getrows();
    this.BankAccount.patchValue({ PRODUCTLIST: row });
    try {
      this._genericService
        .service("employxx/save_delivery", this.BankAccount.value)
        .subscribe(
          (returnData: returnModel) => {
            if (returnData.Success && returnData.StatusCode == 200) {
              //this.mappingTheData(returnData.DataObject);
              // this._spinner.hide();
            } else {
            }
          },
          (error) => {
            console.log(error.status);
          }
        );
    } catch (error) {}
    //
    // if (this.BankAccount.valid) {
    //   this._genericService
    //     .service("DeliveryChallan", this.BankAccount.value)
    //     .subscribe(val => {
    //       debugger;
    //       this.generatePDF();
    //     });
    // } else {
    // }
  }
  generatePDF() {
    // debugger;
    const doc = this.getDocumentDefinition();

    pdfMake.createPdf(doc).open();
  }
  ngOnInit() {
    this.reactiveForm();
    this.getDcNo();
  }
  getDcNo() {
    this._genericService.getService("employxx/get_DeliveryChallan").subscribe(
      (returnData: returnModel) => {
        debugger;
        if (returnData.Success && returnData.StatusCode == 200) {
          if (returnData.DataObject.searchGridData.length > 0) {
            this.patchValue(
              returnData.DataObject.searchGridData[
                returnData.DataObject.searchGridData.length - 1
              ]
            );
          }
        } else {
        }
      },
      (error) => {
        console.log(error.status);
      }
    );
  }
  reactiveForm() {
    this.BankAccount = this.fb.group({
      PurchaseOrderNo: ["", Validators.compose([Validators.required])],
      PurchaseOrderDate: new Date(),
      DCDate: new Date(),
      dcno: 0,
      dcid: 0,
      VehicleNo: [""],
      PRODUCTLIST: [],
    });
  }
  ngOnDestroy() {
    this.alive = false;
  }
  getDocumentDefinition() {
    //sessionStorage.setItem('resume', JSON.stringify(this.resume));<br> ​
    return {
      content: [
        {
          text: "Asim Tin (PVT.) LTD.",
          bold: true,
          fontSize: 25,
          alignment: "center",
          margin: [0, 0, 0, 10],
        },
        {
          text: "Plot No.E-9/A, S.I.T.E., Karachi, Pakistan",
          margin: [0, 0, 0, 5],
          alignment: "center",
        },
        {
          text: "Tel:(92-21)32569454-5, Email:AsimTin@gmail.com",
          margin: [0, 0, 0, 5],
          alignment: "center",
        },
        {
          text: "DELIVERY CHALLAN",
          margin: [0, 0, 10, 5],
          style: "name",
          alignment: "center",
        },
        {
          columns: [
            [
              {
                text:
                  "Purchase Order No." +
                  this.BankAccount.controls.PurchaseOrderNo.value,
                margin: [0, 0, 10, 5],
              },
              {
                text: `Purchase Order Date. ${this.BankAccount.controls.PurchaseOrderDate.value}`,
                margin: [0, 0, 0, 10],
              },

              // {
              //   text: "GitHub: " + "this.resume.socialProfile",
              //   link: "this.resume.socialProfile",
              //   color: "blue"
              // }
            ],
            [
              {
                text: "ORIGINAL",
                decoration: "underline",
                alignment: "center",
              },
              {
                text: `D.C No: ${this.BankAccount.controls.dcid.value}`,
              },
              {
                text: `D.C Date: ${this.BankAccount.controls.DCDate.value}`,
              },

              {
                // text: `Vehicle No: ${(Math.random() * 1000).toFixed(0)}`
                text: `Vehicle No: ${this.BankAccount.controls.VehicleNo.value}`,
              },
            ],
          ],
        },

        {
          text: "Order Details",
          decoration: "underline",
          margin: [0, 0, 10, 5],
        },
        {
          table: {
            headerRows: 1,
            widths: ["auto", "*", "auto", "auto", "*"],
            body: [
              ["S.No", "Description of item", "Unit", "Quantity", "Remarks"],
              // ...this.invoice.products.map(p => ([p.name, p.price, p.qty, (p.price * p.qty).toFixed(2)])),
              // [{ text: 'Total Amount', colSpan: 3 }, {}, {}, this.invoice.products.reduce((sum, p) => sum + (p.qty * p.price), 0).toFixed(2)]
            ],
          },
        },
        {
          text: `Totals: ${100}`,
          alignment: "right",
          margin: [10, 10, 0, 10],
        },
        {
          text: "Signature",
          style: "sign",
        },
      ],

      styles: {
        name: {
          fontSize: 16,
          bold: true,
        },
        sign: {
          margin: [0, 50, 0, 10],
          alignment: "right",
          italics: true,
        },
      },
    };
  }
  @ViewChild(addrepgridcomponent, { static: true })
  frmPhone: addrepgridcomponent;
  addrow() {
    let row = this.frmPhone.myGrid.getrows().length;
    let datarow = this.frmPhone.generaterow(row + 1);
    this.frmPhone.myGrid.addrow(null, datarow);
  }
  deleterow() {
    if (this.frmPhone.myGrid.getrows().length != 0) {
      let selectedrowindex = this.frmPhone.myGrid.getselectedrowindex();
      let id = this.frmPhone.myGrid.getrowid(selectedrowindex);
      this.frmPhone.myGrid.deleterow(id);
    }
  }
}
