import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { JournalRoutingModule } from "./journal-routing.module";
import { JournalComponent } from "./journal.component";
import { JournaladdComponent } from "./journaladd/journaladd.component";
import { JournallistComponent } from "./journallist/journallist.component";
import { JournalmodifyComponent } from "./journalmodify/journalmodify.component";
import { share } from "rxjs/operators";
import { shareModule } from "app/@core/service/shareModule";

@NgModule({
  declarations: [
    JournalComponent,
    JournaladdComponent,
    JournallistComponent,
    JournalmodifyComponent,
  ],
  imports: [shareModule, JournalRoutingModule],

  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class JournalModule {}
