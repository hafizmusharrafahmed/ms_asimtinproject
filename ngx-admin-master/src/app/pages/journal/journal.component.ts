import { Component, OnInit } from "@angular/core";

@Component({
  selector: "ngx-journal",
  template: ` <router-outlet></router-outlet> `,
})
export class JournalComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}
