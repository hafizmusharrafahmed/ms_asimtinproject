import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { JournalComponent } from "./journal.component";
import { JournaladdComponent } from "./journaladd/journaladd.component";
import { JournallistComponent } from "./journallist/journallist.component";
import { JournalmodifyComponent } from "./journalmodify/journalmodify.component";

const routes: Routes = [
  {
    path: "",
    component: JournalComponent,
    children: [
      {
        path: "Journaladd",
        component: JournaladdComponent,
      },
      {
        path: "Journallist",
        component: JournallistComponent,
      },
      {
        path: "Journalmodify",
        component: JournalmodifyComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class JournalRoutingModule {}
