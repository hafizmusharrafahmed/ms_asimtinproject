import { NbMenuItem } from "@nebular/theme";

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: "E-commerce",
    icon: "shopping-cart-outline",
    link: "/pages/dashboard",
    home: true,
  },
  {
    title: "IoT Dashboard",
    icon: "home-outline",
    link: "/pages/iot-dashboard",
  },
  {
    title: "Forms",
    icon: "edit-2-outline",
    children: [
      {
        title: "Form Inputs",
        link: "/pages/forms/inputs",
      },
      {
        title: "Form Layouts",
        link: "/pages/forms/layouts",
      },
      {
        title: "Buttons",
        link: "/pages/forms/buttons",
      },
      {
        title: "Datepicker",
        link: "/pages/forms/datepicker",
      },
    ],
  },
  {
    title: "Reports",
    group: true,
  },
  {
    title: "Layout",
    icon: "layout-outline",
    children: [
      {
        title: "Stepper",
        link: "/pages/layout/stepper",
      },
      {
        title: "List",
        link: "/pages/layout/list",
      },
      {
        title: "Infinite List",
        link: "/pages/layout/infinite-list",
      },
      {
        title: "Accordion",
        link: "/pages/layout/accordion",
      },
      {
        title: "Tabs",
        pathMatch: "prefix",
        link: "/pages/layout/tabs",
      },
    ],
  },

  // {
  //   title: "General Journal",
  //   icon: "edit-2-outline",
  //   children: [
  //     {
  //       title: "Cash Receipt Voucher",
  //       link: "/pages/general/CashReceipt",
  //     },
  //     {
  //       title: "Cash Payment Voucher",
  //       link: "/pages/general/CashPayment",
  //     },
  //     {
  //       title: "Bank Deposit Voucher",
  //       link: "/pages/general/BankDeposit",
  //     },
  //     {
  //       title: "Bank Payment Voucher",
  //       link: "/pages/general/BankPayment",
  //     },
  //     {
  //       title: "Debit Note",
  //       link: "/pages/general/CashPayment",
  //     },
  //     {
  //       title: "Credit Note",
  //       link: "/pages/general/CashPayment",
  //     },
  //     {
  //       title: "Journal Entry Voucher",
  //       link: "/pages/general/CashPayment",
  //     },
  //     {
  //       title: "Purchases",
  //       link: "/pages/general/CashPayment",
  //     },
  //     {
  //       title: "Purchase Return",
  //       link: "/pages/general/CashPayment",
  //     },
  //     {
  //       title: "Sale Invoice",
  //       link: "/pages/general/CashPayment",
  //     },
  //     {
  //       title: "Sale Returns",
  //       link: "/pages/general/CashPayment",
  //     },
  //     {
  //       title: "Adjusting Entries",
  //       link: "/pages/general/CashPayment",
  //     },
  //     {
  //       title: "Adjusting Entries Reversal",
  //       link: "/pages/general/CashPayment",
  //     },
  //     {
  //       title: "All Type of Vouchers",
  //       link: "/pages/general/CashPayment",
  //     },
  //   ],
  // },

  {
    title: "Delivery Challane",
    icon: "edit-2-outline",
    children: [
      {
        title: "New Challane",
        link: "/pages/challane/deliveryChalane",
      },

      {
        title: "Delivery Challane List",
        link: "/pages/challane/deliveryList",
      },
    ],
  },
  {
    title: "Transectoin",
    group: true,
  },
  {
    title: "Sale",
    icon: "edit-2-outline",
    children: [
      {
        title: "Sale add",
        link: "/pages/sales/salesadd",
      },
      {
        title: "Sale Modify",
        link: "/pages/sales/salesmodify",
      },
      {
        title: "sale List",
        link: "/pages/sales/saleslist",
      },
    ],
  },
  {
    title: "Purchase",
    icon: "edit-2-outline",
    children: [
      {
        title: "Purchase add",
        link: "/pages/Purchase/Purchaseadd",
      },
      {
        title: "PurchaseModify",
        link: "/pages/Purchase/Purchasemodify",
      },
      {
        title: "Purchase List",
        link: "/pages/Purchase/Purchaselist",
      },
    ],
  },
  {
    title: "Payment",
    icon: "edit-2-outline",
    children: [
      {
        title: "Payment add",
        link: "/pages/Payment/Paymentadd",
      },
      {
        title: "Payment Modify",
        link: "/pages/Payment/Paymentmodify",
      },
      {
        title: "Payment List",
        link: "/pages/Payment/Paymentlist",
      },
    ],
  },
  {
    title: "Receipt",
    icon: "edit-2-outline",
    children: [
      {
        title: "Receipt add",
        link: "/pages/Receipt/Receiptadd",
      },
      {
        title: "Receipt Modify",
        link: "/pages/Receipt/Receiptmodify",
      },
      {
        title: "Payment List",
        link: "/pages/Receipt/Receiptlist",
      },
    ],
  },
  {
    title: "Journal",
    icon: "edit-2-outline",
    children: [
      {
        title: "Journal add",
        link: "/pages/Journal/Journaladd",
      },
      {
        title: "Journal Modify",
        link: "/pages/Journal/Journalmodify",
      },
      {
        title: "Journal List",
        link: "/pages/Journal/Journallist",
      },
    ],
  },
  {
    title: "Contra",
    icon: "edit-2-outline",
    children: [
      {
        title: "Contra add",
        link: "/pages/Contra/Contraadd",
      },
      {
        title: "Contra Modify",
        link: "/pages/Contra/Contramodify",
      },
      {
        title: "Contra List",
        link: "/pages/Contra/Contralist",
      },
    ],
  },

  // {
  //   title: "Voucher",
  //   icon: "edit-2-outline",
  //   children: [
  //     {
  //       title: "Cash Receipt Voucher",
  //       link: "",
  //     },
  //     {
  //       title: "Cash Payment Voucher",
  //       link: "",
  //     },
  //     {
  //       title: "Bank Deposit Voucher",
  //       link: "",
  //     },
  //     {
  //       title: "Bank Payment Voucher",
  //       link: "",
  //     },
  //     {
  //       title: "Debit Note",
  //       link: "",
  //     },
  //     {
  //       title: "Credit Note",
  //       link: "",
  //     },
  //     {
  //       title: "Journal Entry Voucher",
  //       link: "",
  //     },
  //     {
  //       title: "Cheque Printing",
  //       link: "",
  //     },
  //   ],
  // },
  // {
  //   title: "Financial",
  //   icon: "edit-2-outline",
  //   children: [
  //     {
  //       title: "General Ledger",
  //       link: "",
  //     },
  //     {
  //       title: "Trial Balance",
  //       link: "",
  //     },
  //     {
  //       title: "Activity Trial Balance",
  //       link: "",
  //     },
  //     {
  //       title: "Work Sheet",
  //       link: "",
  //     },
  //     {
  //       title: "Cash Book",
  //       link: "",
  //     },
  //     {
  //       title: "Bank Reconsliation Statment",
  //       link: "",
  //     },
  //     {
  //       title: "Financial Statment",
  //       link: "",
  //     },
  //   ],
  // },
  // {
  //   title: "Vendot & Purchases",
  //   icon: "edit-2-outline",
  //   children: [
  //     {
  //       title: "Purchase Day Book",
  //       link: "",
  //     },
  //     {
  //       title: "Supplier Wise Report",
  //       link: "",
  //     },
  //     {
  //       title: "Payable Age Analyzing",
  //       link: "",
  //     },
  //     {
  //       title: "Advance Payment Report",
  //       link: "",
  //     },
  //     {
  //       title: "Purchase Summary",
  //       link: "",
  //     },
  //   ],
  // },
  // {
  //   title: "Inventory & Services",
  //   icon: "edit-2-outline",
  //   children: [
  //     {
  //       title: "Item Ledger Report",
  //       link: "",
  //     },
  //     {
  //       title: "Item Activity Balance",
  //       link: "",
  //     },
  //     {
  //       title: "Item Balance",
  //       link: "",
  //     },
  //     {
  //       title: "Advance Payment Report",
  //       link: "",
  //     },
  //     {
  //       title: "Purchase Summary",
  //       link: "",
  //     },
  //   ],
  // },
  // {
  //   title: "UI Features",
  //   icon: "keypad-outline",
  //   link: "/pages/ui-features",
  //   children: [
  //     {
  //       title: "Grid",
  //       link: "/pages/ui-features/grid",
  //     },
  //     {
  //       title: "Icons",
  //       link: "/pages/ui-features/icons",
  //     },
  //     {
  //       title: "Typography",
  //       link: "/pages/ui-features/typography",
  //     },
  //     {
  //       title: "Animated Searches",
  //       link: "/pages/ui-features/search-fields",
  //     },
  //   ],
  // },
  // {
  //   title: "Modal & Overlays",
  //   icon: "browser-outline",
  //   children: [
  //     {
  //       title: "Dialog",
  //       link: "/pages/modal-overlays/dialog",
  //     },
  //     {
  //       title: "Window",
  //       link: "/pages/modal-overlays/window",
  //     },
  //     {
  //       title: "Popover",
  //       link: "/pages/modal-overlays/popover",
  //     },
  //     {
  //       title: "Toastr",
  //       link: "/pages/modal-overlays/toastr",
  //     },
  //     {
  //       title: "Tooltip",
  //       link: "/pages/modal-overlays/tooltip",
  //     },
  //   ],
  // },
  // {
  //   title: "Extra Components",
  //   icon: "message-circle-outline",
  //   children: [
  //     {
  //       title: "Calendar",
  //       link: "/pages/extra-components/calendar",
  //     },
  //     {
  //       title: "Progress Bar",
  //       link: "/pages/extra-components/progress-bar",
  //     },
  //     {
  //       title: "Spinner",
  //       link: "/pages/extra-components/spinner",
  //     },
  //     {
  //       title: "Alert",
  //       link: "/pages/extra-components/alert",
  //     },
  //     {
  //       title: "Calendar Kit",
  //       link: "/pages/extra-components/calendar-kit",
  //     },
  //     {
  //       title: "Chat",
  //       link: "/pages/extra-components/chat",
  //     },
  //   ],
  // },
  // {
  //   title: "Maps",
  //   icon: "map-outline",
  //   children: [
  //     {
  //       title: "Google Maps",
  //       link: "/pages/maps/gmaps",
  //     },
  //     {
  //       title: "Leaflet Maps",
  //       link: "/pages/maps/leaflet",
  //     },
  //     {
  //       title: "Bubble Maps",
  //       link: "/pages/maps/bubble",
  //     },
  //     {
  //       title: "Search Maps",
  //       link: "/pages/maps/searchmap",
  //     },
  //   ],
  // },
  // {
  //   title: "Charts",
  //   icon: "pie-chart-outline",
  //   children: [
  //     {
  //       title: "Echarts",
  //       link: "/pages/charts/echarts",
  //     },
  //     {
  //       title: "Charts.js",
  //       link: "/pages/charts/chartjs",
  //     },
  //     {
  //       title: "D3",
  //       link: "/pages/charts/d3",
  //     },
  //   ],
  // },
  {
    title: "Editors",
    icon: "text-outline",
    children: [
      {
        title: "TinyMCE",
        link: "/pages/editors/tinymce",
      },
      {
        title: "CKEditor",
        link: "/pages/editors/ckeditor",
      },
    ],
  },
  {
    title: "Tables & Data",
    icon: "grid-outline",
    children: [
      {
        title: "Smart Table",
        link: "/pages/tables/smart-table",
      },
      {
        title: "Tree Grid",
        link: "/pages/tables/tree-grid",
      },
    ],
  },
  // {
  //   title: "Miscellaneous",
  //   icon: "shuffle-2-outline",
  //   children: [
  //     {
  //       title: "404",
  //       link: "/pages/miscellaneous/404",
  //     },
  //   ],
  // },
  // {
  //   title: "Auth",
  //   icon: "lock-outline",
  //   children: [
  //     {
  //       title: "Login",
  //       link: "/auth/login",
  //     },
  //     {
  //       title: "Register",
  //       link: "/auth/register",
  //     },
  //     {
  //       title: "Request Password",
  //       link: "/auth/request-password",
  //     },
  //     {
  //       title: "Reset Password",
  //       link: "/auth/reset-password",
  //     },
  //   ],
  // },
];
