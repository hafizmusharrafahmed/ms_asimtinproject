import { Component, OnInit } from "@angular/core";

@Component({
  selector: "ngx-contra",
  template: ` <router-outlet></router-outlet> `,
})
export class ContraComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}
