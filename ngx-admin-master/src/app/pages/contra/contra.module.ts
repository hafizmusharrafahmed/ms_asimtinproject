import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { ContraRoutingModule } from "./contra-routing.module";
import { ContraComponent } from "./contra.component";
import { ContaaddComponent } from "./contaadd/contaadd.component";
import { ContamodifyComponent } from "./contamodify/contamodify.component";
import { ContalistComponent } from "./contalist/contalist.component";
import { shareModule } from "app/@core/service/shareModule";

@NgModule({
  declarations: [
    ContraComponent,
    ContaaddComponent,
    ContamodifyComponent,
    ContalistComponent,
  ],
  imports: [shareModule, ContraRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class ContraModule {}
