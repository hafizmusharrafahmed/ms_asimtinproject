import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { ContaaddComponent } from "./contaadd/contaadd.component";
import { ContalistComponent } from "./contalist/contalist.component";
import { ContamodifyComponent } from "./contamodify/contamodify.component";
import { ContraComponent } from "./contra.component";

const routes: Routes = [
  {
    path: "",
    component: ContraComponent,
    children: [
      {
        path: "Contraadd",
        component: ContaaddComponent,
      },
      {
        path: "Contralist",
        component: ContalistComponent,
      },
      {
        path: "Contramodify",
        component: ContamodifyComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ContraRoutingModule {}
