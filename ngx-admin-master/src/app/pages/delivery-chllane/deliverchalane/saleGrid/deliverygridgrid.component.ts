import {
  Component,
  ViewChild,
  Output,
  EventEmitter,
  Input,
  OnChanges,
} from "@angular/core";
import { jqxGridComponent } from "jqwidgets-ng/jqxgrid";

@Component({
  selector: "deliverygrid",
  template: `
    <jqxGrid
      #myGrid
      [width]="'100%'"
      [height]="250"
      [source]="dataAdapter"
      [columns]="columns"
      [autoheight]="false"
      [selectionmode]="'singlerow'"
      [editable]="true"
      [altrows]="true"
      [enabletooltips]="true"
      (onRowselect)="myGridOnRowSelect($event)"
      (onRowdoubleclick)="Rowdoubleclick($event)"
      (onCellendedit)="cellendedit($event)"
    >
    </jqxGrid>
  `,
})
export class deliverygridComponent {
  @Input() data: any;
  @ViewChild("myGrid", { static: true }) myGrid: jqxGridComponent;
  @Output()
  GridRowNumberEvent: EventEmitter<any> = new EventEmitter<any>();
  @Output()
  ondoubleClick: EventEmitter<any> = new EventEmitter<any>();
  source: any = {
    localdata: [],
    datatype: "json",
    datafields: [
      { name: "pvSno", type: "number" },
      { name: "pvitem", type: "string" },
      { name: "pvQty", type: "number" },
      { name: "pvUnit", type: "number" },
      { name: "pvPrice", type: "number" },
      { name: "pcAmount", type: "string" },
    ],
  };
  dataAdapter: any = new jqx.dataAdapter(this.source);

  cellsrenderer = (
    row: number,
    columnfield: string,
    value: string | number,
    defaulthtml: string,
    columnproperties: any,
    rowdata: any
  ): string => {
    if (value < 20) {
      return `<span style='margin: 4px; float:${columnproperties.cellsalign}; color: #ff0000;'>${value}</span>`;
    } else {
      return `<span style='margin: 4px; float:${columnproperties.cellsalign}; color: #008000;'>${value}</span>`;
    }
  };

  columns: any[] = [
    { text: "Quality", datafield: "pvSno", width: "40%", editable: false },
    { text: "Size", datafield: "pvitem", width: "20%" },
    //{ text: "Unit", datafield: "pvUnit", width: "10%" },

    { text: "Quantity", datafield: "pvPrice", width: "20%" },
    {
      text: "Weight KGS",
      datafield: "pcAmount",
      width: "20%",
      editable: false,
    },
  ];
  reloadData(gridData: any) {
    var grdLocaldata = [];
    if (gridData != "DataNotFound") {
      grdLocaldata = gridData;
    }
    this.source.localdata = grdLocaldata;
    this.dataAdapter = new jqx.dataAdapter(this.source);
  }
  generaterow(idnum): any {
    let row = {};

    row["pvSno"] = idnum;
    row["pvitem"] = "";
    row["pvQty"] = 0;
    row["pvUnit"] = 0;
    row["pvPrice"] = "";
    row["pcAmount"] = "";

    return row;
  }
  myGridOnRowSelect(event: any): void {
    let args = event.args;
    let row = this.myGrid.getrowdata(args.rowindex);
    this.GridRowNumberEvent.emit(row);
  }

  Rowdoubleclick(event) {
    let args = event.args;
    let row = this.myGrid.getrowdata(args.rowindex);
    this.ondoubleClick.emit(row);
  }
  pvQty = 0;
  price = 0;
  cellendedit(event) {
    if (event.args.type != "api") {
      // let RowId = this.myGrid.getrowid(event.args.rowindex);
      if (event.args.datafield == "pvQty") {
        this.pvQty = 0;
        let val = event.args.value;

        this.pvQty = parseFloat(val == "" ? 0 : val);
      }
      if (event.args.datafield == "pvPrice") {
        this.price = 0;
        this.price = parseFloat(event.args.value == "" ? 0 : event.args.value);
      }
      if (this.price != 0 && this.pvQty != 0) {
        this.myGrid.setcellvalue(
          event.args.rowindex,
          "pcAmount",
          this.price * this.pvQty
        );
      } else {
        //this.myGrid.setcellvalue(event.args.rowindex, "pcAmount", 0);
      }
    }
  }
}
