import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { DeliverchalaneComponent } from "./deliverchalane/deliverchalane.component";
import { DeliverchalaneListComponent } from "./deliverchalane-list/deliverchalane-list.component";

import { shareModule } from "app/@core/service/shareModule";
import { challaneModule } from "./sale-routing.module";
import { ChallaneComponent } from "./challane.component";
import { deliverygridComponent } from "./deliverchalane/saleGrid/deliverygridgrid.component";

@NgModule({
  declarations: [
    ChallaneComponent,
    DeliverchalaneComponent,
    DeliverchalaneListComponent,
    deliverygridComponent,
  ],
  imports: [challaneModule, shareModule],

  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class DeliveryChllaneModule {}
