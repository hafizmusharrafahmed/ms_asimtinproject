import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeliverchalaneListComponent } from './deliverchalane-list.component';

describe('DeliverchalaneListComponent', () => {
  let component: DeliverchalaneListComponent;
  let fixture: ComponentFixture<DeliverchalaneListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeliverchalaneListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeliverchalaneListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
