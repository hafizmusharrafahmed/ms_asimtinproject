import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ChallaneComponent } from "./challane.component";
import { DeliverchalaneListComponent } from "./deliverchalane-list/deliverchalane-list.component";
import { DeliverchalaneComponent } from "./deliverchalane/deliverchalane.component";

const routes: Routes = [
  {
    path: "",
    component: ChallaneComponent,
    children: [
      {
        path: "deliveryChalane",
        component: DeliverchalaneComponent,
      },

      {
        path: "deliveryList",
        component: DeliverchalaneListComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class challaneModule {}
