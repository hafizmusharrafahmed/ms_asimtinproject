import { Component, OnInit } from "@angular/core";

@Component({
  selector: "ngx-receipt",
  template: ` <router-outlet></router-outlet> `,
})
export class ReceiptComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}
