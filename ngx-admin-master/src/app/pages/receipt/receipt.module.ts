import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from "@angular/core";

import { ReceiptRoutingModule } from "./receipt-routing.module";
import { ReceiptComponent } from "./receipt.component";
import { ReceiptaddComponent } from "./receiptadd/receiptadd.component";
import { ReceiptmodifyComponent } from "./receiptmodify/receiptmodify.component";
import { ReceiptlistComponent } from "./receiptlist/receiptlist.component";
import { shareModule } from "app/@core/service/shareModule";
import { receiptaddgridcomponent } from "./receiptadd/repeiptGrid/repeiptaddgrid.component";
import { receiptListgridComponent } from "./receiptlist/receiptlistGrid/receiptListgrid.component";

@NgModule({
  declarations: [
    ReceiptComponent,
    ReceiptaddComponent,
    ReceiptmodifyComponent,
    ReceiptlistComponent,
    receiptaddgridcomponent,
    receiptListgridComponent,
  ],
  imports: [shareModule, ReceiptRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class ReceiptModule {}
