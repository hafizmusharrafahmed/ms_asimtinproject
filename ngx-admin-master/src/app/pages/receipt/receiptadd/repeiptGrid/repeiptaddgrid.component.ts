import {
  Component,
  ViewChild,
  Output,
  EventEmitter,
  Input,
  OnChanges,
} from "@angular/core";
import { jqxGridComponent } from "jqwidgets-ng/jqxgrid";

@Component({
  selector: "receiptaddgrid",
  template: `
    <jqxGrid
      #myGrid
      [width]="'100%'"
      [height]="250"
      [source]="dataAdapter"
      [columns]="columns"
      [autoheight]="false"
      [selectionmode]="'singlerow'"
      [editable]="true"
      [altrows]="true"
      [enabletooltips]="true"
      (onRowselect)="myGridOnRowSelect($event)"
      (onRowdoubleclick)="Rowdoubleclick($event)"
      (onCellendedit)="cellendedit($event)"
    >
    </jqxGrid>
  `,
})
export class receiptaddgridcomponent {
  @Input() data: any;
  @ViewChild("myGrid", { static: true }) myGrid: jqxGridComponent;
  @Output()
  GridRowNumberEvent: EventEmitter<any> = new EventEmitter<any>();
  @Output()
  ondoubleClick: EventEmitter<any> = new EventEmitter<any>();
  source: any = {
    localdata: [],
    datatype: "json",
    datafields: [
      { name: "receiptvSno", type: "number" },
      { name: "receiptvitem", type: "string" },
      { name: "receiptvQty", type: "number" },
      // { name: "pvUnit", type: "number" },
      { name: "receiptvPrice", type: "number" },
      { name: "receiptvId", type: "number" },
      { name: "svAmount", type: "string" },
    ],
  };
  dataAdapter: any = new jqx.dataAdapter(this.source);

  cellsrenderer = (
    row: number,
    columnfield: string,
    value: string | number,
    defaulthtml: string,
    columnproperties: any,
    rowdata: any
  ): string => {
    if (value < 20) {
      return `<span style='margin: 4px; float:${columnproperties.cellsalign}; color: #ff0000;'>${value}</span>`;
    } else {
      return `<span style='margin: 4px; float:${columnproperties.cellsalign}; color: #008000;'>${value}</span>`;
    }
  };

  columns: any[] = [
    {
      text: "receiptvSno",
      datafield: "receiptvSno",
      width: "5%",
      editable: false,
    },
    { text: "Description of item", datafield: "receiptvitem", width: "35%" },
    //{ text: "Unit", datafield: "pvUnit", width: "10%" },
    { text: "Quantity", datafield: "receiptvQty", width: "20%" },
    { text: "Price", datafield: "receiptvPrice", width: "20%" },
    { text: "Amount", datafield: "svAmount", width: "20%", editable: false },
  ];
  reloadData(gridData: any) {
    var grdLocaldata = [];
    if (gridData != "DataNotFound") {
      grdLocaldata = gridData;
    }
    this.source.localdata = grdLocaldata;
    this.dataAdapter = new jqx.dataAdapter(this.source);
  }
  generaterow(idnum): any {
    let row = {};

    row["receiptvSno"] = idnum;
    row["receiptvitem"] = "";
    row["receiptvQty"] = 0;
    //row["pvUnit"] = 0;
    row["receiptvPrice"] = "";
    row["svAmount"] = "";

    return row;
  }
  myGridOnRowSelect(event: any): void {
    let args = event.args;

    let row = this.myGrid.getrowdata(args.rowindex);
    this.GridRowNumberEvent.emit(row);
  }

  Rowdoubleclick(event) {
    let args = event.args;

    let row = this.myGrid.getrowdata(args.rowindex);
    this.ondoubleClick.emit(row);
  }
  receiptvQty = 0;
  price = 0;
  cellendedit(event) {
    if (event.args.type != "api") {
      // let RowId = this.myGrid.getrowid(event.args.rowindex);

      //   let RowData = this.myGrid.getrowdata(event.args.rowindex);

      if (event.args.datafield == "receiptvQty") {
        this.receiptvQty = 0;
        let val = event.args.value;

        this.receiptvQty = parseFloat(val == "" ? 0 : val);
      }
      if (event.args.datafield == "receiptvPrice") {
        this.price = 0;
        this.price = parseFloat(event.args.value == "" ? 0 : event.args.value);
      }
      if (this.price != 0 && this.receiptvQty != 0) {
        this.myGrid.setcellvalue(
          event.args.rowindex,
          "svAmount",
          this.price * this.receiptvQty
        );
      } else {
        //this.myGrid.setcellvalue(event.args.rowindex, "svAmount", 0);
      }
    }
  }
}
