import {
  Component,
  ViewChild,
  Output,
  EventEmitter,
  Input,
  OnChanges,
} from "@angular/core";
import { jqxGridComponent } from "jqwidgets-ng/jqxgrid";

@Component({
  selector: "receiptListgrid",
  template: `
    <jqxGrid
      #myGrid
      [width]="'100%'"
      [height]="250"
      [source]="dataAdapter"
      [columns]="columns"
      [autoheight]="true"
      [autorowheight]="false"
      [pageable]="true"
      [selectionmode]="'singlerow'"
      [editable]="false"
      [altrows]="true"
      [filterable]="true"
      [showfilterrow]="true"
      [columnsresize]="true"
      [enabletooltips]="true"
      (onRowselect)="myGridOnRowSelect($event)"
      (onRowdoubleclick)="Rowdoubleclick($event)"
      (onFilter)="Filter($event)"
    >
    </jqxGrid>
  `,
})
export class receiptListgridComponent {
  @Input() data: any;
  @ViewChild("myGrid", { static: true }) myGrid: jqxGridComponent;
  @Output()
  GridRowNumberEvent: EventEmitter<any> = new EventEmitter<any>();
  @Output()
  ondoubleClick: EventEmitter<any> = new EventEmitter<any>();
  source: any = {
    localdata: [],
    datatype: "json",
    datafields: [
      { name: "receiptDate", type: "date" },
      { name: "receiptId", type: "string" },
      { name: "pvpurchaseSeriesQty", type: "string" },
      { name: "receiptParty", type: "string" },
      { name: "receiptNarration", type: "string" },
      { name: "receiptVH_no", type: "string" },
      { name: "svAmount", type: "string" },
      { name: "receiptvPrice", type: "string" },
      { name: "receiptSeries", type: "string" },
    ],
  };
  dataAdapter: any = new jqx.dataAdapter(this.source);

  cellsrenderer = (
    row: number,
    columnfield: string,
    value: string | number,
    defaulthtml: string,
    columnproperties: any,
    rowdata: any
  ): string => {
    if (value < 20) {
      return `<span style='margin: 4px; float:${columnproperties.cellsalign}; color: #ff0000;'>${value}</span>`;
    } else {
      return `<span style='margin: 4px; float:${columnproperties.cellsalign}; color: #008000;'>${value}</span>`;
    }
  };

  columns: any[] = [
    { text: "receiptId", datafield: "receiptId", width: "10%" },
    { text: "receiptSeries", datafield: "receiptSeries", width: "15%" },
    { text: "receiptParty", datafield: "receiptParty", width: "10%" },
    { text: "receiptNarration", datafield: "receiptNarration", width: "20%" },
    {
      text: "receiptDate",
      datafield: "receiptDate",
      width: "15%",
      cellsFormat: "MM/dd/yyyy",
      cellsalign: "right",
      columntype: "datetimeinput",
      filtertype: "date",
    },

    { text: "receiptVh_No", datafield: "receiptVH_no", width: "10%" },
    { text: "Amount", datafield: "svAmount", width: "10%" },
    { text: "Price", datafield: "receiptvPrice", width: "10%" },
  ];
  reloadData(gridData: any) {
    var grdLocaldata = [];
    if (gridData != "DataNotFound") {
      grdLocaldata = gridData;
    }
    this.source.localdata = grdLocaldata;
    this.dataAdapter = new jqx.dataAdapter(this.source);
  }
  generaterow(idnum): any {
    let row = {};

    row["pvSno"] = idnum;
    row["pvitem"] = "";
    row["pvQty"] = "";
    row["pvUnit"] = 0;
    row["receiptvPrice"] = "";
    row["pcAmount"] = "";

    return row;
  }
  myGridOnRowSelect(event: any): void {
    let args = event.args;

    let row = this.myGrid.getrowdata(args.rowindex);
    this.GridRowNumberEvent.emit(row);
  }

  Rowdoubleclick(event) {
    let args = event.args;
    let row = this.myGrid.getrowdata(args.rowindex);
    this.ondoubleClick.emit(row);
  }

  // awardsVMModel: awardsVM;
  // @Output()
  // awardsVmModelEmit: EventEmitter<awardsVM> = new EventEmitter<awardsVM>();
  Filter(event: any): void {
    // this.awardsVMModel = new awardsVM();
    // let rows = this.myGrid.getrows();
    // if (rows.length == 0 && event.args.filters.length > 0) {
    //   if (event.args.filters.length > 0) {
    //     let rows = event.args.filters.length;
    //     for (let index = 0; index < rows; index++) {
    //       let datafield = event.args.filters[index].datafield;
    //       if (datafield == "YEAR") {
    //         let data = event.args.filters[index].filter.getfilters()[0].value;
    //         this.awardsVMModel.YEAR = data;
    //         this.awardsVmModelEmit.emit(this.awardsVMModel);
    //       } else if (datafield == "DESC") {
    //         let data = event.args.filters[index].filter.getfilters()[0].value;
    //         this.awardsVMModel.DESC = data;
    //         this.awardsVmModelEmit.emit(this.awardsVMModel);
    //       } else if (datafield == "AMOUNT") {
    //         let data = event.args.filters[index].filter.getfilters()[0].value;
    //         this.awardsVMModel.AMOUNT = data;
    //         this.awardsVmModelEmit.emit(this.awardsVMModel);
    //       }
    //     }
    //   }
    // } else {
    //   if (event.args.filters.length > 0) {
    //     let rows = event.args.filters.length;
    //     for (let index = 0; index < rows; index++) {
    //       let datafield = event.args.filters[index].datafield;
    //       if (datafield == "YEAR") {
    //         let data = event.args.filters[index].filter.getfilters()[0].value;
    //         this.awardsVMModel.YEAR = data;
    //         this.awardsVmModelEmit.emit(this.awardsVMModel);
    //       } else if (datafield == "DESC") {
    //         let data = event.args.filters[index].filter.getfilters()[0].value;
    //         this.awardsVMModel.DESC = data;
    //         this.awardsVmModelEmit.emit(this.awardsVMModel);
    //       } else if (datafield == "AMOUNT") {
    //         let data = event.args.filters[index].filter.getfilters()[0].value;
    //         this.awardsVMModel.AMOUNT = data;
    //         this.awardsVmModelEmit.emit(this.awardsVMModel);
    //       }
    //     }
    //   }
    // }
  }
}
