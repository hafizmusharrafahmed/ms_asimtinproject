import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { ReceiptComponent } from "./receipt.component";
import { ReceiptaddComponent } from "./receiptadd/receiptadd.component";
import { ReceiptlistComponent } from "./receiptlist/receiptlist.component";
import { ReceiptmodifyComponent } from "./receiptmodify/receiptmodify.component";

const routes: Routes = [
  {
    path: "",
    component: ReceiptComponent,
    children: [
      {
        path: "Receiptadd",
        component: ReceiptaddComponent,
      },
      {
        path: "Receiptlist",
        component: ReceiptlistComponent,
      },
      {
        path: "Receiptmodify",
        component: ReceiptmodifyComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReceiptRoutingModule {}
