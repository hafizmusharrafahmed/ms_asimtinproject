import { Component, OnInit, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { returnModel } from "../../../@core/service/HandlerModel";
import { genericService } from "../../../@core/service/genericService";
import pdfMake from "pdfmake/build/pdfmake";
import pdfFonts from "pdfmake/build/vfs_fonts";
import { Router } from "@angular/router";
import { paymentListgridComponent } from "./paymentlistGrid/paymentListgrid.component";

pdfMake.vfs = pdfFonts.pdfMake.vfs;
@Component({
  selector: "ngx-paymentlist",
  templateUrl: "./paymentlist.component.html",
  styleUrls: ["./paymentlist.component.scss"],
  providers: [genericService],
})
export class PaymentlistComponent implements OnInit {
  @ViewChild(paymentListgridComponent) mygrid: paymentListgridComponent;

  constructor(
    private _genericService: genericService,

    private fb: FormBuilder,
    private router: Router
  ) {}

  ngOnInit() {
    this.getSaleList();
  }
  getArray = [];
  getSaleList() {
    this._genericService
      .getService("AgentModule/get_saleList?id=" + 0)
      .subscribe(
        (returnData: returnModel) => {
          if (returnData.Success && returnData.StatusCode == 200) {
            if (returnData.DataObject.searchGridData.length > 0) {
              this.getArray = returnData.DataObject.searchGridData;
              this.mygrid.reloadData(this.getArray);
            }
          } else {
          }
        },
        (error) => {
          console.log(error.status);
        }
      );
  }
  excelBtnOnClick() {
    this.mygrid.myGrid.exportdata("xls", "jqxGrid");
  }
  pdfBtnOnClick() {
    this.mygrid.myGrid.exportdata("pdf", "jqxGrid");
  }
  doubleClick(event) {
    if (event !== null) {
      let id = event.purchaseId;
      this.router.navigate(["/pages/sales/salesmodify"], {
        queryParams: { id: id },
      });
    }
  }
}
