import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { PaymentRoutingModule } from "./payment-routing.module";
import { PaymentComponent } from "./payment.component";
import { PaymentaddComponent } from "./paymentadd/paymentadd.component";
import { PaymentlistComponent } from "./paymentlist/paymentlist.component";
import { PaymentmodifyComponent } from "./paymentmodify/paymentmodify.component";
import { shareModule } from "app/@core/service/shareModule";
import { paymentaddgridcomponent } from "./paymentadd/paymentGrid/paymentAddgrid.component";
import { paymentListgridComponent } from "./paymentlist/paymentlistGrid/paymentListgrid.component";

@NgModule({
  declarations: [
    PaymentComponent,
    PaymentaddComponent,
    PaymentlistComponent,
    PaymentmodifyComponent,
    paymentaddgridcomponent,
    paymentListgridComponent,
  ],
  imports: [shareModule, PaymentRoutingModule],

  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class PaymentModule {}
