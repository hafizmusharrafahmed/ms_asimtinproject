import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { PaymentComponent } from "./payment.component";
import { PaymentaddComponent } from "./paymentadd/paymentadd.component";
import { PaymentlistComponent } from "./paymentlist/paymentlist.component";
import { PaymentmodifyComponent } from "./paymentmodify/paymentmodify.component";

const routes: Routes = [
  {
    path: "",
    component: PaymentComponent,
    children: [
      {
        path: "Paymentadd",
        component: PaymentaddComponent,
      },
      {
        path: "Paymentlist",
        component: PaymentlistComponent,
      },
      {
        path: "Paymentmodify",
        component: PaymentmodifyComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PaymentRoutingModule {}
