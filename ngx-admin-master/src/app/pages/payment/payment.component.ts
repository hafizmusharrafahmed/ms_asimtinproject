import { Component, OnInit } from "@angular/core";

@Component({
  selector: "ngx-payment",
  template: ` <router-outlet></router-outlet> `,
})
export class PaymentComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}
