import { Component, OnInit } from "@angular/core";

@Component({
  selector: "ngx-purchases",
  template: ` <router-outlet></router-outlet> `,
})
export class PurchasesComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}
