import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { PurchaseaddComponent } from "./purchaseadd/purchaseadd.component";
import { PurchaselistComponent } from "./purchaselist/purchaselist.component";
import { PurchasemodifyComponent } from "./purchasemodify/purchasemodify.component";
import { PurchasesComponent } from "./purchases.component";

const routes: Routes = [
  {
    path: "",
    component: PurchasesComponent,
    children: [
      {
        path: "Purchaseadd",
        component: PurchaseaddComponent,
      },
      {
        path: "Purchaselist",
        component: PurchaselistComponent,
      },
      {
        path: "Purchasemodify",
        component: PurchasemodifyComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PurchaseRoutingModule {}
