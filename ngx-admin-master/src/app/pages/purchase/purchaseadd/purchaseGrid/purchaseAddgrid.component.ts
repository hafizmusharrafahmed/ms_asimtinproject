import {
  Component,
  ViewChild,
  Output,
  EventEmitter,
  Input,
  OnChanges,
} from "@angular/core";
import { jqxGridComponent } from "jqwidgets-ng/jqxgrid";

@Component({
  selector: "purchaseaddgrid",
  template: `
    <jqxGrid
      #myGrid
      [width]="'100%'"
      [height]="250"
      [source]="dataAdapter"
      [columns]="columns"
      [autoheight]="false"
      [selectionmode]="'singlerow'"
      [editable]="true"
      [altrows]="true"
      [enabletooltips]="true"
      (onRowselect)="myGridOnRowSelect($event)"
      (onRowdoubleclick)="Rowdoubleclick($event)"
      (onCellendedit)="cellendedit($event)"
    >
    </jqxGrid>
  `,
})
export class purchaseaddgridcomponent {
  @Input() data: any;
  @ViewChild("myGrid", { static: true }) myGrid: jqxGridComponent;
  @Output()
  GridRowNumberEvent: EventEmitter<any> = new EventEmitter<any>();
  @Output()
  ondoubleClick: EventEmitter<any> = new EventEmitter<any>();
  source: any = {
    localdata: [],
    datatype: "json",
    datafields: [
      { name: "pvSno", type: "number" },
      { name: "Account", type: "string" },
      { name: "paymentvId", type: "number" },
      { name: "paymentvitem", type: "string" },
      { name: "DC", type: "string" },
      { name: "intNo", type: "string" },
      { name: "intType", type: "string" },
      { name: "credit", type: "string" },
      { name: "debit", type: "string" },
    ],
  };
  dataAdapter: any = new jqx.dataAdapter(this.source);

  cellsrenderer = (
    row: number,
    columnfield: string,
    value: string | number,
    defaulthtml: string,
    columnproperties: any,
    rowdata: any
  ): string => {
    if (value < 20) {
      return `<span style='margin: 4px; float:${columnproperties.cellsalign}; color: #ff0000;'>${value}</span>`;
    } else {
      return `<span style='margin: 4px; float:${columnproperties.cellsalign}; color: #008000;'>${value}</span>`;
    }
  };

  columns: any[] = [
    { text: "S.NO", datafield: "pvSno", width: "5%", editable: false },
    { text: "D/C", datafield: "DC", width: "10%" },
    { text: "Account", datafield: "Account", width: "15%" },
    { text: "Debit", datafield: "debit", width: "10%" },
    { text: "Credit", datafield: "credit", width: "10%" },
    { text: "Ints.Type", datafield: "intType", width: "10%" },
    { text: "Ints. No", datafield: "intNo", width: "20%" },
    {
      text: "Short Narration",
      datafield: "paymentvitem",
      width: "20%",
    },
  ];
  reloadData(gridData: any) {
    var grdLocaldata = [];
    if (gridData != "DataNotFound") {
      grdLocaldata = gridData;
    }
    this.source.localdata = grdLocaldata;
    this.dataAdapter = new jqx.dataAdapter(this.source);
  }
  generaterow(idnum): any {
    let row = {};

    row["pvSno"] = idnum;
    row["Account"] = "";
    row["DC"] = "";
    row["intNo"] = "";
    row["intType"] = "";
    row["paymentvitem"] = "";
    row["debit"] = "";
    row["credit"] = "";

    return row;
  }
  myGridOnRowSelect(event: any): void {
    let args = event.args;

    let row = this.myGrid.getrowdata(args.rowindex);
    this.GridRowNumberEvent.emit(row);
  }

  Rowdoubleclick(event) {
    let args = event.args;

    let row = this.myGrid.getrowdata(args.rowindex);
    this.ondoubleClick.emit(row);
  }
  pvQty = 0;
  price = 0;
  cellendedit(event) {
    if (event.args.type != "api") {
      // let RowId = this.myGrid.getrowid(event.args.rowindex);

      if (event.args.datafield == "pvQty") {
        this.pvQty = 0;
        let val = event.args.value;

        this.pvQty = parseFloat(val == "" ? 0 : val);
      }
      if (event.args.datafield == "pvPrice") {
        this.price = 0;
        this.price = parseFloat(event.args.value == "" ? 0 : event.args.value);
      }
      if (this.price != 0 && this.pvQty != 0) {
        this.myGrid.setcellvalue(
          event.args.rowindex,
          "pcAmount",
          this.price * this.pvQty
        );
      } else {
        //this.myGrid.setcellvalue(event.args.rowindex, "pcAmount", 0);
      }
    }
  }
}
