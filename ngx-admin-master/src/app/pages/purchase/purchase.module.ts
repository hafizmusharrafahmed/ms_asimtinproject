import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from "@angular/core";
import { PurchaseRoutingModule } from "./purchase-routing.module";
import { PurchasesComponent } from "./purchases.component";
import { PurchaseaddComponent } from "./purchaseadd/purchaseadd.component";
import { PurchasemodifyComponent } from "./purchasemodify/purchasemodify.component";
import { PurchaselistComponent } from "./purchaselist/purchaselist.component";
import { shareModule } from "app/@core/service/shareModule";
import { purchaseaddgridcomponent } from "./purchaseadd/purchaseGrid/purchaseAddgrid.component";
import { purchaseListgridComponent } from "./purchaselist/purchaselistGrid/purchaseListgrid.component";

@NgModule({
  imports: [shareModule, PurchaseRoutingModule],
  declarations: [
    PurchasesComponent,
    purchaseaddgridcomponent,
    purchaseListgridComponent,
    PurchaseaddComponent,
    PurchasemodifyComponent,
    PurchaselistComponent,
  ],

  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class PurchaseModule {}
