import {
  Component,
  ViewChild,
  Output,
  EventEmitter,
  Input,
  OnChanges
} from "@angular/core";
import { jqxGridComponent } from "jqwidgets-ng/jqxgrid";

@Component({
  selector: "addrepgrid",
  template: `
    <jqxGrid
      #myGrid
      [width]="'100%'"
      [height]="250"
      [source]="dataAdapter"
      [columns]="columns"
      [autoheight]="false"
      [selectionmode]="'singlerow'"
      [editable]="true"
      [altrows]="true"
      [enabletooltips]="true"
      (onRowselect)="myGridOnRowSelect($event)"
      (onRowdoubleclick)="Rowdoubleclick($event)"
      (onCellendedit)="cellendedit($event)"
    >
    </jqxGrid>
  `
})
export class addrepgridcomponent {
  @Input() data: any;
  @ViewChild("myGrid", { static: true }) myGrid: jqxGridComponent;
  @Output()
  GridRowNumberEvent: EventEmitter<any> = new EventEmitter<any>();
  @Output()
  ondoubleClick: EventEmitter<any> = new EventEmitter<any>();
  source: any = {
    localdata: [],
    datatype: "json",
    datafields: [
      { name: "Sno", type: "number" },

      { name: "Description", type: "string" },
      { name: "unit", type: "number" },
      { name: "quantity", type: "number" },
      { name: "remarks", type: "string" }
    ]
  };
  dataAdapter: any = new jqx.dataAdapter(this.source);

  cellsrenderer = (
    row: number,
    columnfield: string,
    value: string | number,
    defaulthtml: string,
    columnproperties: any,
    rowdata: any
  ): string => {
    if (value < 20) {
      return `<span style='margin: 4px; float:${
        columnproperties.cellsalign
      }; color: #ff0000;'>${value}</span>`;
    } else {
      return `<span style='margin: 4px; float:${
        columnproperties.cellsalign
      }; color: #008000;'>${value}</span>`;
    }
  };

  columns: any[] = [
    { text: "S.NO", datafield: "Sno", width: "5%" },
    { text: "Description of item", datafield: "Description", width: "35%" },
    { text: "Unit", datafield: "unit", width: "10%" },
    { text: "Quantity", datafield: "quantity", width: "20%" },
    { text: "Remarks", datafield: "remarks", width: "30%" }
  ];
  reloadData(gridData: any) {
    var grdLocaldata = [];
    if (gridData != "DataNotFound") {
      grdLocaldata = gridData;
    }
    this.source.localdata = grdLocaldata;
    this.dataAdapter = new jqx.dataAdapter(this.source);
  }
  generaterow(idnum): any {
    let row = {};

    row["Sno"] = idnum;

    row["Description"] = "";
    row["unit"] = "";

    row["quantity"] = 0;
    row["remarks"] = "";

    return row;
  }
  myGridOnRowSelect(event: any): void {
    let args = event.args;

    let row = this.myGrid.getrowdata(args.rowindex);
    this.GridRowNumberEvent.emit(row);
  }

  Rowdoubleclick(event) {
    let args = event.args;

    let row = this.myGrid.getrowdata(args.rowindex);
    this.ondoubleClick.emit(row);
  }
  cellendedit(event) {
    //debugger;
    if (event.args.datafield === "") {
    }

    if (event.args.oldvalue != event.args.value) {
    }
  }
}
