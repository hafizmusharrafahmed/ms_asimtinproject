import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { GeneraljournalComponent } from "./generaljournal.component";
import { CashreceiptvoucherComponent } from "./cashreceiptvoucher/cashreceiptvoucher.component";
import { generalJouralModule } from "./general-routing.module";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { ThemeModule } from "../../@theme/theme.module";

import {
  NbActionsModule,
  NbButtonModule,
  NbCardModule,
  NbCheckboxModule,
  NbDatepickerModule,
  NbIconModule,
  NbInputModule,
  NbRadioModule,
  NbSelectModule,
  NbUserModule
} from "@nebular/theme";
import { CashpaymentvoucherComponent } from "./cashpaymentvoucher/cashpaymentvoucher.component";
import { BankDepositVoucherComponent } from "./bank-deposit-voucher/bank-deposit-voucher.component";
import { BankpayentvoucherComponent } from "./bankpayentvoucher/bankpayentvoucher.component";
import { DebitnoteComponent } from "./debitnote/debitnote.component";
import { CreditnoteComponent } from "./creditnote/creditnote.component";
import { JournalentryvoucherComponent } from "./journalentryvoucher/journalentryvoucher.component";
import { PurchaseComponent } from "./purchase/purchase.component";
import { PurchasereturnComponent } from "./purchasereturn/purchasereturn.component";
import { SalevoiceComponent } from "./salevoice/salevoice.component";
import { SalereturnsComponent } from "./salereturns/salereturns.component";
import { AdjustingentriesreversalComponent } from "./adjustingentriesreversal/adjustingentriesreversal.component";
import { AlltypeofvouchersComponent } from "./alltypeofvouchers/alltypeofvouchers.component";

@NgModule({
  declarations: [
    GeneraljournalComponent,
    CashreceiptvoucherComponent,
    CashpaymentvoucherComponent,
    BankDepositVoucherComponent,
    BankpayentvoucherComponent,
    DebitnoteComponent,
    CreditnoteComponent,
    JournalentryvoucherComponent,
    PurchaseComponent,
    PurchasereturnComponent,
    SalevoiceComponent,
    SalereturnsComponent,
    AdjustingentriesreversalComponent,
    AlltypeofvouchersComponent
  ],
  imports: [
    CommonModule,
    generalJouralModule,
    FormsModule,
    ReactiveFormsModule,
    // jqx grid module is import for temporary
    ThemeModule,
    NbCardModule,
    NbUserModule,
    NbButtonModule,

    NbActionsModule,
    NbRadioModule,
    NbSelectModule,

    NbIconModule,
    NbButtonModule,
    NbInputModule,
    NbCardModule,
    NbButtonModule,
    NbActionsModule,
    NbUserModule,
    NbCheckboxModule,
    NbRadioModule,
    NbDatepickerModule,
    NbSelectModule,
    NbIconModule
  ]
})
export class GenerealjournalModule {}
