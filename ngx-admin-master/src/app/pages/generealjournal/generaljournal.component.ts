import { Component, OnInit } from "@angular/core";

@Component({
  selector: "ngx-generaljournal",
  template: `
    <router-outlet></router-outlet>
  `
})
export class GeneraljournalComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}
