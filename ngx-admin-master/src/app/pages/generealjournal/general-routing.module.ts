import { BankDepositVoucherComponent } from "./bank-deposit-voucher/bank-deposit-voucher.component";
import { BankpayentvoucherComponent } from "./bankpayentvoucher/bankpayentvoucher.component";
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { GeneraljournalComponent } from "./generaljournal.component";
import { CashreceiptvoucherComponent } from "./cashreceiptvoucher/cashreceiptvoucher.component";
import { CashpaymentvoucherComponent } from "./cashpaymentvoucher/cashpaymentvoucher.component";

const routes: Routes = [
  {
    path: "",
    component: GeneraljournalComponent,
    children: [
      {
        path: "CashReceipt",
        component: CashreceiptvoucherComponent
      },
      {
        path: "CashPayment",
        component: CashpaymentvoucherComponent
      },
      {
        path: "BankDeposit",
        component: BankDepositVoucherComponent
      },
      {
        path: "BankPayment",
        component: BankpayentvoucherComponent
      }
      // {
      //   path: "datepicker",
      //   component: DatepickerComponent
      // }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class generalJouralModule {}
