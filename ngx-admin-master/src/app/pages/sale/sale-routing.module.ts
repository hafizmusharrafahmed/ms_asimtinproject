import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { SaleAddComponent } from "./sale-add/sale-add.component";
import { SaleListComponent } from "./sale-list/sale-list.component";
import { SaleModifyComponent } from "./sale-modify/sale-modify.component";
import { SaleComponent } from "./sale.component";

const routes: Routes = [
  {
    path: "",
    component: SaleComponent,
    children: [
      {
        path: "salesadd",
        component: SaleAddComponent,
      },
      {
        path: "saleslist",
        component: SaleListComponent,
      },
      {
        path: "salesmodify",
        component: SaleModifyComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class saleroutingModule {}
