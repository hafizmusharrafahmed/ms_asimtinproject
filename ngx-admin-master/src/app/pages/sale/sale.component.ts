import { Component, OnInit } from "@angular/core";

@Component({
  selector: "ngx-sale",
  template: ` <router-outlet></router-outlet> `,
})
export class SaleComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}
