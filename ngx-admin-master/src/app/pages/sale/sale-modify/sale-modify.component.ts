import { Component, OnInit, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { returnModel } from "../../../@core/service/HandlerModel";
import { genericService } from "../../../@core/service/genericService";
import pdfMake from "pdfmake/build/pdfmake";
import pdfFonts from "pdfmake/build/vfs_fonts";
import {
  NbComponentStatus,
  NbGlobalPhysicalPosition,
  NbGlobalPosition,
  NbToastrConfig,
  NbToastrService,
} from "@nebular/theme";
import { salesaddgridcomponent } from "../sale-add/saleGrid/saleassgrid.component";
import { ActivatedRoute } from "@angular/router";
pdfMake.vfs = pdfFonts.pdfMake.vfs;

@Component({
  selector: "ngx-sale-modify",
  templateUrl: "./sale-modify.component.html",
  styleUrls: ["./sale-modify.component.scss"],
})
export class SaleModifyComponent implements OnInit {
  @ViewChild(salesaddgridcomponent) mygrid: salesaddgridcomponent;
  solarValue: number;

  salesAddForm: FormGroup;

  constructor(
    private _genericService: genericService,

    private fb: FormBuilder,

    private toastrService: NbToastrService,

    private Activatedroute: ActivatedRoute
  ) {}
  config: NbToastrConfig;

  index = 1;
  destroyByClick = true;
  duration = 2000;
  hasIcon = true;
  position: NbGlobalPosition = NbGlobalPhysicalPosition.TOP_RIGHT;
  preventDuplicates = false;
  status: NbComponentStatus = "primary";
  title = "HI there!";
  content = `I'm cool toaster!`;
  makeToast(type: NbComponentStatus, message) {
    const config = {
      status: type,
      destroyByClick: this.destroyByClick,
      duration: this.duration,
      hasIcon: this.hasIcon,
      position: this.position,
      preventDuplicates: this.preventDuplicates,
    };
    const titleContent = this.title ? `. ${this.title}` : "";
    this.toastrService.show(
      message,
      `Toast ${this.index}${titleContent}`,
      config
    );
  }

  submitForm() {
    let row = this.mygrid.myGrid.getrows();
    this.salesAddForm.patchValue({ PRODUCTLIST: row });
    try {
      if (this.salesAddForm.valid) {
        this._genericService
          .service("salemodule/save_saleadd", this.salesAddForm.value)
          .subscribe(
            (returnData: returnModel) => {
              if (returnData.Success && returnData.StatusCode == 200) {
                debugger;
                this.makeToast(this.status, returnData.Message);
                this.salesAddForm.patchValue({
                  purchaseId: returnData.DataObject.purchaseId,
                });
                //  debugger;
              } else {
              }
            },
            (error) => {
              console.log(error.status);
            }
          );
      } else {
      }
    } catch (error) {}
  }
  generatePDF() {
    // debugger;
    const doc = this.getDocumentDefinition();

    pdfMake.createPdf(doc).open();
  }
  ngOnInit() {
    let pageNum = 0;

    this.Activatedroute.queryParamMap.subscribe((params) => {
      pageNum = +params.get("id") || 0;
    });
    this.reactiveForm();
    this.getSeries();
    this.get_partyName();

    if (pageNum != 0) {
      this.getDataOnId(pageNum);
    } else {
    }
  }
  getDataOnId(id) {
    this._genericService
      .getService("salemodule/get_saleList?id=" + id)
      .subscribe(
        (returnData: returnModel) => {
          if (returnData.Success && returnData.StatusCode == 200) {
            if (returnData.DataObject.searchGridData.length > 0) {
              this.mygrid.reloadData(returnData.DataObject.GridData);
              this.patchValue(returnData.DataObject.searchGridData[0]);
            }
          } else {
          }
        },
        (error) => {
          console.log(error.status);
        }
      );
  }
  getSeries() {
    this._genericService.getService("AgentModule/get_Series").subscribe(
      (returnData: returnModel) => {
        if (returnData.Success && returnData.StatusCode == 200) {
          if (returnData.DataObject.searchGridData.length > 0) {
            this.positions = returnData.DataObject.searchGridData;
          }
        } else {
        }
      },
      (error) => {
        console.log(error.status);
      }
    );
  }
  get_partyName() {
    this._genericService.getService("AgentModule/get_partyName").subscribe(
      (returnData: returnModel) => {
        if (returnData.Success && returnData.StatusCode == 200) {
          if (returnData.DataObject.searchGridData.length > 0) {
            this.partyList = returnData.DataObject.searchGridData;
          }
        } else {
        }
      },
      (error) => {
        console.log(error.status);
      }
    );
  }
  patchValue(event) {
    this.salesAddForm.patchValue(event);
  }
  reactiveForm() {
    this.salesAddForm = this.fb.group({
      purchaseId: 0,
      purchaseSeries: ["", Validators.compose([Validators.required])],
      purchaseDate: new Date(),
      purchaseVh_No: "",
      purchaseParty: ["", Validators.compose([Validators.required])],
      purchaseNarration: "",
      purchaseType: "main",
      PRODUCTLIST: [],
    });
  }
  ngOnDestroy() {}
  // it is used for printing
  getDocumentDefinition() {
    return {
      content: [
        {
          text: "Asim Tin (PVT.) LTD.",
          bold: true,
          fontSize: 25,
          alignment: "center",
          margin: [0, 0, 0, 10],
        },
        {
          text: "Plot No.E-9/A, S.I.T.E., Karachi, Pakistan",
          margin: [0, 0, 0, 5],
          alignment: "center",
        },
        {
          text: "Tel:(92-21)32569454-5, Email:AsimTin@gmail.com",
          margin: [0, 0, 0, 5],
          alignment: "center",
        },
        {
          text: "DELIVERY CHALLAN",
          margin: [0, 0, 10, 5],
          style: "name",
          alignment: "center",
        },
        {
          columns: [
            [
              {
                text:
                  "Purchase Order No." +
                  this.salesAddForm.controls.PurchaseOrderNo.value,
                margin: [0, 0, 10, 5],
              },
              {
                text: `Purchase Order Date. ${this.salesAddForm.controls.PurchaseOrderDate.value}`,
                margin: [0, 0, 0, 10],
              },

              // {
              //   text: "GitHub: " + "this.resume.socialProfile",
              //   link: "this.resume.socialProfile",
              //   color: "blue"
              // }
            ],
            [
              {
                text: "ORIGINAL",
                decoration: "underline",
                alignment: "center",
              },
              {
                text: `D.C No: ${this.salesAddForm.controls.dcid.value}`,
              },
              {
                text: `D.C Date: ${this.salesAddForm.controls.DCDate.value}`,
              },

              {
                // text: `Vehicle No: ${(Math.random() * 1000).toFixed(0)}`
                text: `Vehicle No: ${this.salesAddForm.controls.VehicleNo.value}`,
              },
            ],
          ],
        },

        {
          text: "Order Details",
          decoration: "underline",
          margin: [0, 0, 10, 5],
        },
        {
          table: {
            headerRows: 1,
            widths: ["auto", "*", "auto", "auto", "*"],
            body: [
              ["S.No", "Description of item", "Unit", "Quantity", "Remarks"],
              // ...this.invoice.products.map(p => ([p.name, p.price, p.qty, (p.price * p.qty).toFixed(2)])),
              // [{ text: 'Total Amount', colSpan: 3 }, {}, {}, this.invoice.products.reduce((sum, p) => sum + (p.qty * p.price), 0).toFixed(2)]
            ],
          },
        },
        {
          text: `Totals: ${100}`,
          alignment: "right",
          margin: [10, 10, 0, 10],
        },
        {
          text: "Signature",
          style: "sign",
        },
      ],

      styles: {
        name: {
          fontSize: 16,
          bold: true,
        },
        sign: {
          margin: [0, 50, 0, 10],
          alignment: "right",
          italics: true,
        },
      },
    };
  }
  @ViewChild(salesaddgridcomponent, { static: true })
  frmPhone: salesaddgridcomponent;
  addrow() {
    let row = this.frmPhone.myGrid.getrows().length;
    let datarow = this.frmPhone.generaterow(row + 1);
    this.frmPhone.myGrid.addrow(null, datarow);
  }
  deleterow() {
    if (this.frmPhone.myGrid.getrows().length != 0) {
      let selectedrowindex = this.frmPhone.myGrid.getselectedrowindex();
      let id = this.frmPhone.myGrid.getrowid(selectedrowindex);
      this.frmPhone.myGrid.deleterow(id);
    }
  }
  positions: string[] = [];
  partyList: string[] = [];
  clear() {
    this.reactiveForm();
    this.mygrid.reloadData([]);
  }
}
