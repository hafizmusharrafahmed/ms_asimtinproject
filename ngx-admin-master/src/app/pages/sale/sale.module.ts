import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SaleComponent } from "./sale.component";
import { SaleAddComponent } from "./sale-add/sale-add.component";
import { SaleModifyComponent } from "./sale-modify/sale-modify.component";
import { SaleListComponent } from "./sale-list/sale-list.component";

import {
  NbActionsModule,
  NbButtonModule,
  NbCardModule,
  NbCheckboxModule,
  NbDatepickerModule,
  NbIconModule,
  NbInputModule,
  NbRadioModule,
  NbSelectModule,
  NbUserModule,
} from "@nebular/theme";
import { saleroutingModule } from "./sale-routing.module";
import { salesaddgridcomponent } from "./sale-add/saleGrid/saleassgrid.component";
import { jqxGridModule } from "jqwidgets-ng/jqxgrid";
import { shareModule } from "app/@core/service/shareModule";
import { salesListgridComponent } from "./sale-list/salelistGrid/salesListgrid.component";
@NgModule({
  imports: [saleroutingModule, shareModule],
  declarations: [
    SaleComponent,
    SaleAddComponent,
    salesaddgridcomponent,
    SaleModifyComponent,
    SaleListComponent,
    salesListgridComponent,
  ],

  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class SaleModule {}
